import './MissionModal.scss';

const MissionModal = ({ mission }) => {
  return (
    <div className="mission-modal__wrapper">
      <h2>{mission.mission_name}</h2>
      <div className="separator"></div>
      <p>{mission.mission_id}</p>
      <div className="mission-modal__links">
        {mission.wikipedia ? (
          <a href={mission.wikipedia} target="_blank">
            Wiki
          </a>
        ) : null}
        {mission.twitter ? (
          <a href={mission.twitter} target="_blank">
            Tweet
          </a>
        ) : null}
        {mission.website ? (
          <a href={mission.website} target="_blank">
            WWW
          </a>
        ) : null}
      </div>
      <p className="mission-modal__description">{mission.description}</p>
    </div>
  );
};

export default MissionModal;
