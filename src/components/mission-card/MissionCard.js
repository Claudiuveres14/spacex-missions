import './MissionCard.scss';

const MissionCard = ({ missionName, selectMission }) => {
  return (
    <div className="mission__wrapper" onClick={selectMission}>
      {missionName}
    </div>
  );
};

export default MissionCard;
