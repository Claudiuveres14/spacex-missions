import React from 'react';
import './Modal.scss';

const Modal = ({ children, isOpen, close }) => {
  if (!isOpen) {
    return null;
  }
  return (
    <div
      className="modal__overlay"
      onClick={event => {
        event.stopPropagation();
        close();
      }}
    >
      <div
        className="modal__content-wrapper"
        onClick={event => {
          event.stopPropagation();
        }}
      >
        <button className="modal__close-button" onClick={close}>
          X
        </button>
        {children}
      </div>
    </div>
  );
};

export default Modal;
