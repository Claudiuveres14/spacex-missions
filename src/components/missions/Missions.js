import './Missions.scss';
import { useState, useEffect } from 'react';
import MissionCard from '../mission-card/MissionCard';
import { useMissions } from '../../hook/useMissions';
import Modal from '../modal/Modal';
import MissionModal from '../mission-modal/MissionModal';

const Missions = () => {
  const missions = useMissions();
  const [currentMissions, setCurrentMissions] = useState(missions);
  const [query, setQuery] = useState('');
  const [selectedMission, setSelectedMission] = useState(null);

  useEffect(() => {
    updateMissions(query);
  }, [missions]);

  const handleQueryChange = e => {
    const newQuery = e.target.value;
    setQuery(newQuery);

    updateMissions(newQuery);
  };

  function updateMissions(query) {
    const newMissions = missions.filter(mission =>
      mission.mission_name.toLowerCase().includes(query.toLowerCase())
    );

    setCurrentMissions(newMissions);
  }
  return (
    <section className="missions-section__wrapper">
      <p>These are the missions from SpaceX (: </p>
      <input
        type="text"
        value={query}
        name="details"
        onChange={handleQueryChange}
        placeholder="Search for mission"
        className="missions-section__input"
      />
      <div className="missions__wrapper">
        {currentMissions.map(mission => (
          <MissionCard
            selectMission={() => {
              setSelectedMission(mission);
            }}
            missionName={mission.mission_name}
            key={mission.mission_name}
          />
        ))}
      </div>

      <Modal
        isOpen={selectedMission}
        close={() => {
          setSelectedMission(null);
        }}
      >
        <MissionModal mission={selectedMission} />
      </Modal>
    </section>
  );
};

export default Missions;
