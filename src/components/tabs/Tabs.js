import './Tabs.scss';
import { useState } from 'react';

const Tabs = ({ tabs }) => {
  const [currentTab, setCurrentTab] = useState(() => {
    const initialTab = tabs.find(tab => {
      return tab.intialTab;
    });
    return initialTab || tabs[0];
  });

  return (
    <>
      <div className="tabs__wrapper">
        {tabs.map(tab => {
          return <h2 className="tab__header">{tab.title}</h2>;
        })}
      </div>
      {currentTab.component}
    </>
  );
};

export default Tabs;
