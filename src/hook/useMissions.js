import { useEffect, useState } from 'react';

export const useMissions = () => {
  const [missions, setMissions] = useState([]);

  useEffect(() => {
    fetch('https://api.spacexdata.com/v3/missions')
      .then(response => {
        const responeJson = response.json();
        return responeJson;
      })
      .then(missionsData => setMissions(missionsData));
  }, []);

  return missions;
};
