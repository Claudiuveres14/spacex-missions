import './App.scss';
import Missions from './components/missions/Missions';
import Tabs from './components/tabs/Tabs';

const tabs = [
  {
    title: 'Missions',
    component: <Missions />,
    initialTab: true,
  },
  {
    title: 'Launches',
    component: <Missions />,
    initialTab: true,
  },
];

function App() {
  return (
    <div className="wrapper">
      <h1 className="title">SpaceX Missions</h1>
      <div className="separator"></div>
      <Tabs tabs={tabs} />
    </div>
  );
}

export default App;
